namespace alefair.libraries.S4B.Activities
{

    using System;
    using System.Activities;
    using System.Threading;
    using System.Threading.Tasks;
    using alefair.libraries.S4B;
    using alefair.libraries.S4B.Activities;
    using alefair.libraries.S4B.Activities.Methods;
    using alefair.libraries.S4B.Activities.Properties;
    using Microsoft.Lync.Model;
    using Microsoft.Lync.Model.Conversation;
    using System.Collections.Generic;
    using System.Activities.Statements;
    using UiPath.Shared.Activities;
    using UiPath.Shared.Activities.Localization;
    using UiPath.Shared.Activities.Utilities;


    public enum MessageType
    {
        PlainText,
        Html,
        RichText
    }

    [LocalizedDisplayName(nameof(Resources.S4BSendMessage_DisplayName))]
    [LocalizedDescription(nameof(Resources.S4BSendMessage_Description))]
    public class S4BSendMessage : ContinuableAsyncCodeActivity
    {
        #region Properties

        /// <summary>
        /// If set, continue executing the remaining activities even if the current activity has failed.
        /// </summary>
        [LocalizedCategory(nameof(Resources.Common_Category))]
        [LocalizedDisplayName(nameof(Resources.ContinueOnError_DisplayName))]
        [LocalizedDescription(nameof(Resources.ContinueOnError_Description))]
        public override InArgument<bool> ContinueOnError { get; set; }

        [LocalizedDisplayName(nameof(Resources.S4BSendMessage_EmailList_DisplayName))]
        [LocalizedDescription(nameof(Resources.S4BSendMessage_EmailList_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<String[]> EmailList { get; set; }

        [LocalizedDisplayName(nameof(Resources.S4BSendMessage_Message_DisplayName))]
        [LocalizedDescription(nameof(Resources.S4BSendMessage_Message_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<string> Message { get; set; }

        [LocalizedDisplayName(nameof(Resources.S4BSendMessage_TypeMessage_DisplayName))]
        [LocalizedDescription(nameof(Resources.S4BSendMessage_TypeMessage_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public MessageType TypeMessage { get; set; }

        #endregion


        #region Constructors

        public S4BSendMessage()
        {
        }

        #endregion


        #region Protected Methods

        protected override void CacheMetadata(CodeActivityMetadata metadata)
        {
            if (EmailList == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(EmailList)));
            if (Message == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(Message)));

            base.CacheMetadata(metadata);
        }

        protected override async Task<Action<AsyncCodeActivityContext>> ExecuteAsync(AsyncCodeActivityContext context, CancellationToken cancellationToken)
        {
            // Inputs
            var emailList = EmailList.Get(context);
            var message = Message.Get(context);
            var typeMessage = TypeMessage;
            LyncClient gClient = LyncClient.GetClient();


            ///////////////////////////
            try
            {
                Conversation gConversation = gClient.ConversationManager.AddConversation();

                foreach (string recepient in emailList)
                {
                    Methods.Methods.CheckAndAddToFavorite(gClient.ContactManager, recepient);
                    gConversation.AddParticipant(gClient.ContactManager.GetContactByUri("sip:" + recepient));
                }

                InstantMessageModality imModality = gConversation.Modalities[ModalityTypes.InstantMessage] as InstantMessageModality;

                //messageType
                IDictionary<InstantMessageContentType, string> textMessage = new Dictionary<InstantMessageContentType, string>();
                textMessage.Add((InstantMessageContentType)typeMessage, message);

                imModality.BeginSendMessage(textMessage, null, null);

                gConversation = null;
            }
            catch (Exception ex)
            {
                throw new ApplicationException(ex.Message.ToString());
            }

            // Outputs
            return (ctx) => {
            };
        }

        #endregion
    }
}

