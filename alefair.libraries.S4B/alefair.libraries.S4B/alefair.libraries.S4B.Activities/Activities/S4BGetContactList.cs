using System;
using System.Activities;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using alefair.libraries.S4B.Activities.Methods;
using alefair.libraries.S4B.Activities.Properties;
using UiPath.Shared.Activities;
using UiPath.Shared.Activities.Localization;
using Microsoft.Lync;
using Microsoft.Lync.Model;
using Microsoft.Lync.Model.Group;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UiPath.Shared.Activities.Utilities;
using System.Activities.Statements;

namespace alefair.libraries.S4B.Activities
{
    [LocalizedDisplayName(nameof(Resources.S4BGetContactList_DisplayName))]
    [LocalizedDescription(nameof(Resources.S4BGetContactList_Description))]
    public class S4BGetContactList : ContinuableAsyncCodeActivity
    {
        #region Properties

        /// <summary>
        /// If set, continue executing the remaining activities even if the current activity has failed.
        /// </summary>
        [LocalizedCategory(nameof(Resources.Common_Category))]
        [LocalizedDisplayName(nameof(Resources.ContinueOnError_DisplayName))]
        [LocalizedDescription(nameof(Resources.ContinueOnError_Description))]
        public override InArgument<bool> ContinueOnError { get; set; }

        [LocalizedDisplayName(nameof(Resources.S4BGetContactList_ContactList_DisplayName))]
        [LocalizedDescription(nameof(Resources.S4BGetContactList_ContactList_Description))]
        [LocalizedCategory(nameof(Resources.Output_Category))]
        public OutArgument<JArray> ContactList { get; set; }

        #endregion


        #region Constructors

        public S4BGetContactList()
        {
        }

        #endregion


        #region Protected Methods

        protected override void CacheMetadata(CodeActivityMetadata metadata)
        {

            base.CacheMetadata(metadata);
        }

        protected override async Task<Action<AsyncCodeActivityContext>> ExecuteAsync(AsyncCodeActivityContext context, CancellationToken cancellationToken)
        {
            // Inputs
            LyncClient gClient = LyncClient.GetClient();

            JArray addressBook = new JArray();

            ///////////////////////////
            try
            {
                foreach (Group group in gClient.ContactManager.Groups)
                {
                    foreach (Contact contact in group)
                    {
                        addressBook.Add(JToken.FromObject(Methods.Methods.GetAccountInformation(contact)));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new System.ApplicationException("S4B ContactList Method Error: " + ex.Message.ToString() + "; Source: " + ex.Source.ToString());
            }
            ///////////////////////////

            // Outputs
            return (ctx) => {
                ContactList.Set(ctx, addressBook);
            };
        }

        #endregion
    }
}

