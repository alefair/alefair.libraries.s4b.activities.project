using System;
using System.Activities;
using System.Threading;
using System.Threading.Tasks;
using System.Collections.Generic;
using alefair.libraries.S4B.Activities.Methods;
using alefair.libraries.S4B.Activities.Properties;
using Microsoft.Lync;
using Microsoft.Lync.Model;
using Microsoft.Lync.Model.Group;
using Newtonsoft;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using UiPath.Shared.Activities;
using UiPath.Shared.Activities.Localization;
using UiPath.Shared.Activities.Utilities;

namespace alefair.libraries.S4B.Activities
{
    [LocalizedDisplayName(nameof(Resources.S4BGetContactInformation_DisplayName))]
    [LocalizedDescription(nameof(Resources.S4BGetContactInformation_Description))]
    public class S4BGetContactInformation : ContinuableAsyncCodeActivity
    {
        #region Properties

        /// <summary>
        /// If set, continue executing the remaining activities even if the current activity has failed.
        /// </summary>
        [LocalizedCategory(nameof(Resources.Common_Category))]
        [LocalizedDisplayName(nameof(Resources.ContinueOnError_DisplayName))]
        [LocalizedDescription(nameof(Resources.ContinueOnError_Description))]
        public override InArgument<bool> ContinueOnError { get; set; }

        [LocalizedDisplayName(nameof(Resources.S4BSendMessage_EmailList_DisplayName))]
        [LocalizedDescription(nameof(Resources.S4BSendMessage_EmailList_Description))]
        [LocalizedCategory(nameof(Resources.Input_Category))]
        public InArgument<String[]> EmailList { get; set; }

        [LocalizedDisplayName(nameof(Resources.S4BGetContactList_ContactList_DisplayName))]
        [LocalizedDescription(nameof(Resources.S4BGetContactList_ContactList_Description))]
        [LocalizedCategory(nameof(Resources.Output_Category))]
        public OutArgument<JArray> ContactList { get; set; }

        #endregion


        #region Constructors

        public S4BGetContactInformation()
        {
        }

        #endregion


        #region Protected Methods

        protected override void CacheMetadata(CodeActivityMetadata metadata)
        {
            if (EmailList == null) metadata.AddValidationError(string.Format(Resources.ValidationValue_Error, nameof(EmailList)));

            base.CacheMetadata(metadata);
        }

        protected override async Task<Action<AsyncCodeActivityContext>> ExecuteAsync(AsyncCodeActivityContext context, CancellationToken cancellationToken)
        {
            // Inputs
            var emailList = EmailList.Get(context);

            LyncClient gClient = LyncClient.GetClient();

            JArray addressBook = new JArray();

            ///////////////////////////
            try
            {
                foreach (string contact in emailList)
                {
                    Methods.Methods.CheckAndAddToFavorite(gClient.ContactManager, contact);
                    addressBook.Add(JToken.FromObject(Methods.Methods.GetAccountInformation(gClient.ContactManager.GetContactByUri("sip:" + contact))));
                }
            }
            catch (Exception ex)
            {
                throw new System.ApplicationException("S4B GetContactInformation Method Error: " + ex.Message.ToString() + "; Source: " + ex.Source.ToString());
            }
            ///////////////////////////

            // Outputs
            return (ctx) => {
                ContactList.Set(ctx, addressBook);
            };
        }

        #endregion
    }
}

