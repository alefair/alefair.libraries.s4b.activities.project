﻿//------------------------------------------------------------------------------
// <auto-generated>
//     Этот код создан программой.
//     Исполняемая версия:4.0.30319.42000
//
//     Изменения в этом файле могут привести к неправильной работе и будут потеряны в случае
//     повторной генерации кода.
// </auto-generated>
//------------------------------------------------------------------------------

namespace alefair.libraries.S4B.Activities.Properties {
    using System;
    
    
    /// <summary>
    ///   Класс ресурса со строгой типизацией для поиска локализованных строк и т.д.
    /// </summary>
    // Этот класс создан автоматически классом StronglyTypedResourceBuilder
    // с помощью такого средства, как ResGen или Visual Studio.
    // Чтобы добавить или удалить член, измените файл .ResX и снова запустите ResGen
    // с параметром /str или перестройте свой проект VS.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("alefair.libraries.S4B.Activities.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Authentication.
        /// </summary>
        public static string Authentication_Category {
            get {
                return ResourceManager.GetString("Authentication_Category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Common.
        /// </summary>
        public static string Common_Category {
            get {
                return ResourceManager.GetString("Common_Category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на If set, continue executing the remaining activities even if the current activity has failed..
        /// </summary>
        public static string ContinueOnError_Description {
            get {
                return ResourceManager.GetString("ContinueOnError_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на ContinueOnError.
        /// </summary>
        public static string ContinueOnError_DisplayName {
            get {
                return ResourceManager.GetString("ContinueOnError_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Do.
        /// </summary>
        public static string Do {
            get {
                return ResourceManager.GetString("Do", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Input.
        /// </summary>
        public static string Input_Category {
            get {
                return ResourceManager.GetString("Input_Category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Options.
        /// </summary>
        public static string Options_Category {
            get {
                return ResourceManager.GetString("Options_Category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Output.
        /// </summary>
        public static string Output_Category {
            get {
                return ResourceManager.GetString("Output_Category", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Result.
        /// </summary>
        public static string S4BGetContactInformation_ContactList_Description {
            get {
                return ResourceManager.GetString("S4BGetContactInformation_ContactList_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Contact List.
        /// </summary>
        public static string S4BGetContactInformation_ContactList_DisplayName {
            get {
                return ResourceManager.GetString("S4BGetContactInformation_ContactList_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Get information from contact FullName, Email, Company and etc..
        /// </summary>
        public static string S4BGetContactInformation_Description {
            get {
                return ResourceManager.GetString("S4BGetContactInformation_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на S4B GetContactInformation.
        /// </summary>
        public static string S4BGetContactInformation_DisplayName {
            get {
                return ResourceManager.GetString("S4BGetContactInformation_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Email list aka {&quot;user@example.com&quot;}.
        /// </summary>
        public static string S4BGetContactInformation_EmailList_Description {
            get {
                return ResourceManager.GetString("S4BGetContactInformation_EmailList_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на EmailList.
        /// </summary>
        public static string S4BGetContactInformation_EmailList_DisplayName {
            get {
                return ResourceManager.GetString("S4BGetContactInformation_EmailList_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Contact List in JArray object.
        /// </summary>
        public static string S4BGetContactList_ContactList_Description {
            get {
                return ResourceManager.GetString("S4BGetContactList_ContactList_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Contact List.
        /// </summary>
        public static string S4BGetContactList_ContactList_DisplayName {
            get {
                return ResourceManager.GetString("S4BGetContactList_ContactList_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Get Contact List of S4B Client From all Group.
        /// </summary>
        public static string S4BGetContactList_Description {
            get {
                return ResourceManager.GetString("S4BGetContactList_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на S4B GetContactList.
        /// </summary>
        public static string S4BGetContactList_DisplayName {
            get {
                return ResourceManager.GetString("S4BGetContactList_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Send Message to Users From EmailList.
        /// </summary>
        public static string S4BSendMessage_Description {
            get {
                return ResourceManager.GetString("S4BSendMessage_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на S4B SendMessage.
        /// </summary>
        public static string S4BSendMessage_DisplayName {
            get {
                return ResourceManager.GetString("S4BSendMessage_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Email List aka {&quot;user@example.com&quot;}.
        /// </summary>
        public static string S4BSendMessage_EmailList_Description {
            get {
                return ResourceManager.GetString("S4BSendMessage_EmailList_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на EmailList.
        /// </summary>
        public static string S4BSendMessage_EmailList_DisplayName {
            get {
                return ResourceManager.GetString("S4BSendMessage_EmailList_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Message for User.
        /// </summary>
        public static string S4BSendMessage_Message_Description {
            get {
                return ResourceManager.GetString("S4BSendMessage_Message_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Message.
        /// </summary>
        public static string S4BSendMessage_Message_DisplayName {
            get {
                return ResourceManager.GetString("S4BSendMessage_Message_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Type Message: Plain, HTML and etc....
        /// </summary>
        public static string S4BSendMessage_TypeMessage_Description {
            get {
                return ResourceManager.GetString("S4BSendMessage_TypeMessage_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Type Message.
        /// </summary>
        public static string S4BSendMessage_TypeMessage_DisplayName {
            get {
                return ResourceManager.GetString("S4BSendMessage_TypeMessage_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Specifies the amount of time (in milliseconds) to wait for the activity to run before an error is thrown. The default value is 60000 (1 minute)..
        /// </summary>
        public static string Timeout_Description {
            get {
                return ResourceManager.GetString("Timeout_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Timeout (milliseconds).
        /// </summary>
        public static string Timeout_DisplayName {
            get {
                return ResourceManager.GetString("Timeout_DisplayName", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на The activity timed out and was canceled..
        /// </summary>
        public static string Timeout_Error {
            get {
                return ResourceManager.GetString("Timeout_Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Please provide a value either for {0} or {1}, but not both..
        /// </summary>
        public static string ValidationExclusiveProperties_Error {
            get {
                return ResourceManager.GetString("ValidationExclusiveProperties_Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Please provide values for both {0} and {1}..
        /// </summary>
        public static string ValidationOverloadGroup_Error {
            get {
                return ResourceManager.GetString("ValidationOverloadGroup_Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Cannot be used outside of a {0}..
        /// </summary>
        public static string ValidationScope_Error {
            get {
                return ResourceManager.GetString("ValidationScope_Error", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Ищет локализованную строку, похожую на Please provide a value for {0}..
        /// </summary>
        public static string ValidationValue_Error {
            get {
                return ResourceManager.GetString("ValidationValue_Error", resourceCulture);
            }
        }
    }
}
