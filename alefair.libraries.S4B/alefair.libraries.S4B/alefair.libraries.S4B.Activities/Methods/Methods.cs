﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Forms;
using Microsoft.Lync.Model;
using Microsoft.Lync.Model.Conversation;
using Microsoft.Lync.Model.Conversation.Sharing;
using Microsoft.Lync.Model.Internal;
using Microsoft.Lync.Model.Extensibility;
using Microsoft.Lync.Model.Group;
using Microsoft.Lync.Utilities;
using Newtonsoft.Json.Linq;

namespace alefair.libraries.S4B.Activities.Methods
{

    public static class Methods
    {
        public static string Result;

        public static int WordCount(this String str)
        {
            return str.Split(new char[] { ' ', '.', '?' },
                             StringSplitOptions.RemoveEmptyEntries).Length;
        }

        //
        //GetAccountInformation
        //
        /// <summary>
        /// Find all information about contact
        /// </summary>
        /// <param name="contact">contact name aka 'user@example.com'</param>
        /// <returns>JObject with information with FullName, Email, Company and etc...</returns>
        public static JObject GetAccountInformation(Contact contact)
        {
            if (contact != null)
                try
                {
                    JObject jNewUser = new JObject();

                    jNewUser.Add("StatusID", JToken.FromObject(contact.GetContactInformation(ContactInformationType.ActivityId).ToString()));
                    jNewUser.Add("StatusDescription", JToken.FromObject(contact.GetContactInformation(ContactInformationType.Activity).ToString()));
                    jNewUser.Add("Company", JToken.FromObject(contact.GetContactInformation(ContactInformationType.Company).ToString()));
                    jNewUser.Add("Department", JToken.FromObject(contact.GetContactInformation(ContactInformationType.Department).ToString()));
                    jNewUser.Add("Description", JToken.FromObject(contact.GetContactInformation(ContactInformationType.Description).ToString()));
                    jNewUser.Add("FullName", JToken.FromObject(contact.GetContactInformation(ContactInformationType.DisplayName).ToString()));
                    jNewUser.Add("FirstName", JToken.FromObject(contact.GetContactInformation(ContactInformationType.FirstName).ToString()));
                    jNewUser.Add("LastName", JToken.FromObject(contact.GetContactInformation(ContactInformationType.LastName).ToString()));
                    jNewUser.Add("Email", JToken.FromObject(contact.GetContactInformation(ContactInformationType.EmailAddresses)));
                    jNewUser.Add("LocationName", JToken.FromObject(contact.GetContactInformation(ContactInformationType.LocationName).ToString()));
                    jNewUser.Add("Office", JToken.FromObject(contact.GetContactInformation(ContactInformationType.Office).ToString()));
                    jNewUser.Add("Title", JToken.FromObject(contact.GetContactInformation(ContactInformationType.Title).ToString()));

                    return jNewUser;
                }
                catch (Exception ex)
                {
                    throw new System.ApplicationException("GetAccountInformation Error: " + ex.Message.ToString() + "; Source: " + ex.Source.ToString());
                }
            else
                return null;
        }

        //
        //CheckAndAddToFavorite
        //
        /// <summary>
        /// Check contact in contact List.
        /// If contact don't exist then add to contact List
        /// </summary>
        /// <param name="contactManager">ContactManager object with contact list</param>
        /// <param name="contact">contact name aka 'user@example.com'</param>
        /// <returns>No return result</returns>
        public static void CheckAndAddToFavorite(ContactManager contactManager, string contact)
        {
            try
            {
                Contact gContact;
                Group gFavorite = contactManager.Groups.GetGroupsByType(GroupType.FavoriteContacts).First();
                if (!gFavorite.TryGetContact("sip:" + contact, out gContact))
                    AddContact(contactManager.GetContactByUri("sip:" + contact), gFavorite);
            }
            catch (Exception ex)
            {
                throw new System.ApplicationException("CheckAndAddToAdressBook Error: " + ex.Message.ToString() + "; Source: " + ex.Source.ToString());
            }
        }

        //
        //AddContact
        //
        /// <summary>
        /// Add contact to Contact Group
        /// </summary>
        /// <param name="contact">contact name aka 'user@example.com'</param>
        /// <param name="groupAdd">Group for adding</param>
        /// <returns>No return result</returns>
        public static void AddContact(Contact contact, Group groupAdd)
        {
            if (groupAdd.Type == GroupType.DistributionGroup)
            {
                return;
            }
            if (groupAdd.Contains(contact) == false)
            {
                groupAdd.ContactAdded += group_ContactAdded;
                IAsyncResult ar = groupAdd.BeginAddContact(contact, null, null);

                //UI thread execution blocked until operation completes.
                groupAdd.EndAddContact(ar);
            }
        }

        //
        //group_ContactAdded
        //
        /// <summary>
        /// AddContact callBack
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="data">data</param>
        /// <returns>No return result</returns>
        public static void group_ContactAdded(Object source, GroupMemberChangedEventArgs data)
        {
            ((Group)source).ContactAdded -= group_ContactAdded;
            // Update application UI with event notification.
        }


        public static void GetConversations()
        {
            LyncClient _lyncClient;
            ConversationManager _conversationManager;

            _lyncClient = LyncClient.GetClient();
            _conversationManager = _lyncClient.ConversationManager;
            _conversationManager.ConversationAdded += ConversationAdded;
        }

        private static void ConversationAdded(object sender, ConversationManagerEventArgs e)
        {
            var conversation = e.Conversation;
            conversation.ParticipantAdded += ParticipantAdded;
        }

        private static void ParticipantAdded(object sender, ParticipantCollectionChangedEventArgs e)
        {
            var participant = e.Participant;
            if (participant.IsSelf)
            {
                return;
            }

            var instantMessageModality =
                e.Participant.Modalities[ModalityTypes.InstantMessage] as InstantMessageModality;
            instantMessageModality.InstantMessageReceived += InstantMessageReceived;
        }

        private static void InstantMessageReceived(object sender, MessageSentEventArgs e)
        {
            var text = e.Text.Replace(Environment.NewLine, string.Empty);
            (sender as InstantMessageModality).BeginSendMessage(text, null, null);

            Result = text;
        }
    }
}