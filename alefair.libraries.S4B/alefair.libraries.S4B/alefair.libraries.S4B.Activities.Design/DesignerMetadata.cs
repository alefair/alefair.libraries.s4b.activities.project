using System.Activities.Presentation.Metadata;
using System.ComponentModel;
using System.ComponentModel.Design;
using alefair.libraries.S4B.Activities.Design.Designers;
using alefair.libraries.S4B.Activities.Design.Properties;

namespace alefair.libraries.S4B.Activities.Design
{
    public class DesignerMetadata : IRegisterMetadata
    {
        public void Register()
        {
            var builder = new AttributeTableBuilder();
            builder.ValidateTable();

            var categoryAttribute = new CategoryAttribute($"{Resources.Category}");

            builder.AddCustomAttributes(typeof(S4BSendMessage), categoryAttribute);
            builder.AddCustomAttributes(typeof(S4BSendMessage), new DesignerAttribute(typeof(S4BSendMessageDesigner)));
            builder.AddCustomAttributes(typeof(S4BSendMessage), new HelpKeywordAttribute(""));

            builder.AddCustomAttributes(typeof(S4BGetContactList), categoryAttribute);
            builder.AddCustomAttributes(typeof(S4BGetContactList), new DesignerAttribute(typeof(S4BGetContactListDesigner)));
            builder.AddCustomAttributes(typeof(S4BGetContactList), new HelpKeywordAttribute(""));

            builder.AddCustomAttributes(typeof(S4BGetContactInformation), categoryAttribute);
            builder.AddCustomAttributes(typeof(S4BGetContactInformation), new DesignerAttribute(typeof(S4BGetContactInformationDesigner)));
            builder.AddCustomAttributes(typeof(S4BGetContactInformation), new HelpKeywordAttribute(""));


            MetadataStore.AddAttributeTable(builder.CreateTable());
        }
    }
}
